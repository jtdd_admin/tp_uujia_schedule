<?php


namespace uujia\schedule\time;


use uujia\schedule\cache\CacheAgent;
use uujia\schedule\data\ScheduleData;
use uujia\schedule\traits\InstanceTrait;

class TimeComparator {
	use InstanceTrait;
	
	/**
	 * 是否到期
	 * @var bool
	 */
	protected $_due = false;
	
	/**
	 * 比较（和缓存时间比较 是否到期）
	 *
	 * Date: 2021/3/29
	 * Time: 0:13
	 *
	 * @param int   $taskId
	 * @param array $scheduleItem
	 */
	public function comparate($taskId, $scheduleItem) {
		$this->_due = false;
		
		$_timeData = $scheduleItem['time'] ?? [];
		
		$_timeType = $_timeData['type'] ?? 0;
		$_timeParam = $_timeData['time'] ?? 0;
		
		// 获取缓存
		$_cache = CacheAgent::get('taskId:' . $taskId, []);
		
		if (!empty($_cache)) {
			$_timestamp = $_cache['next_time']['timestamp'] ?? 0;
			if ($_timestamp != 0) {
				// todo: 是否设置窗口期 超过一定时间 自动复位
				$this->_due = time() >= $_timestamp;
				
				if (!$this->_due) {
					return $this;
				}
			}
		}
		
		switch ($_timeType) {
			case ScheduleData::TIME_TYPE_EVERY_MINUTE:
				$_minute = $_timeParam['minute'] ?? 0;
				if ($_minute == 0) {
					CacheAgent::rm('taskId:' . $taskId);
				} else {
					$_timeNext = strtotime(date('Y-m-d H:i:00') . ' +' . $_minute . ' minute');
					CacheAgent::set('taskId:' . $taskId, $_timeNext);
				}
				break;
			
			case ScheduleData::TIME_TYPE_EVERY_HOUR:
				$_hour = $_timeParam['hour'] ?? 0;
				if ($_hour == 0) {
					CacheAgent::rm('taskId:' . $taskId);
				} else {
					$_timeNext = strtotime(date('Y-m-d H:i:00') . ' +' . $_hour . ' hour');
					CacheAgent::set('taskId:' . $taskId, $_timeNext);
				}
				break;
		}
		
		return $this;
	}
	
	/**
	 * @return bool
	 */
	public function isDue(): bool {
		return $this->_due;
	}
	
}