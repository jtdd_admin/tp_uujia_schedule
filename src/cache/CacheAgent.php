<?php


namespace uujia\schedule\cache;


use think\Cache;

/**
 * Class CacheAgent
 * Date: 2021/3/28
 * Time: 18:17
 *
 * 缓存代理
 *
 * @package uujia\schedule\cache
 */
class CacheAgent {
	
	/**
	 * 判断缓存是否存在
	 * @access public
	 * @param  string $name 缓存变量名
	 * @return bool
	 */
	public static function has($name) {
		return Cache::tag('schedule')->has($name);
	}
	
	/**
	 * 读取缓存
	 * @access public
	 * @param  string $name    缓存标识
	 * @param  mixed  $default 默认值
	 * @return mixed
	 */
	public static function get($name, $default = false) {
		return Cache::tag('schedule')->get($name, $default);
	}
	
	/**
	 * 写入缓存
	 * @access public
	 * @param  string   $name   缓存标识
	 * @param  mixed    $value  存储数据
	 * @param  int|null $expire 有效时间 0为永久
	 * @return boolean
	 */
	public static function set($name, $value, $expire = null) {
		return Cache::tag('schedule')->set($name, $value, $expire);
	}
	
	/**
	 * 删除缓存
	 * @access public
	 * @param  string $name 缓存标识
	 * @return boolean
	 */
	public static function rm($name) {
		return Cache::tag('schedule')->rm($name);
	}
	
	/**
	 * 如果不存在则写入缓存
	 *
	 * @access public
	 * @param string $name   缓存变量名
	 * @param mixed  $value  存储数据
	 * @param null   $expire 有效时间 0为永久
	 * @return mixed
	 * @throws \Throwable
	 */
	public static function remember($name, $value, $expire = null) {
		return Cache::tag('schedule')->remember($name, $value, $expire);
	}
	
	/**
	 * 清除缓存
	 *
	 * @access public
	 * @param  string $tag 标签名
	 * @return boolean
	 */
	public static function clear($tag = null) {
		return Cache::clear('schedule');
	}
	
}