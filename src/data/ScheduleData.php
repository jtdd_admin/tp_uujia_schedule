<?php

namespace uujia\schedule\data;

use think\Console;
use uujia\schedule\traits\InstanceTrait;

/**
 * Class ScheduleData
 * Date: 2021/3/28
 * Time: 18:19
 *
 * @package uujia\schedule\data
 */
class ScheduleData {
	
	/**
	 * 命令类型
	 */
	const COMMAND_TYPE_COMMAND = 1; // 命令
	const COMMAND_TYPE_CLOSURE = 2; // 闭包
	
	/**
	 * 时间规则类型
	 * todo: 目前只支持 分、时 其他要支持可能要整体重构 解析cron表达式
	 */
	// const TIME_TYPE_EVERY_SECOND   = 1; // 暂不支持秒级
	const TIME_TYPE_EVERY_MINUTE   = 2; // 每分
	const TIME_TYPE_EVERY_HOUR     = 3; // 每小时
	// const TIME_TYPE_EVERY_DAY      = 4; // 每天
	// const TIME_TYPE_EVERY_MONTH    = 5; // 每月
	// const TIME_TYPE_EVERY_YEAR     = 6; // 每年
	// const TIME_TYPE_EVERY_WEEK     = 7; // 每周
	// const TIME_TYPE_CUSTOMIZE_TIME = 8; // 自定义时间
	
	/**
	 * 任务列表
	 *
	 * @var array
	 */
	protected $_list = [];
	
	
	/**
	 * ScheduleData constructor.
	 */
	public function __construct() {
	
	}
	
	/**
	 * load
	 *
	 * Date: 2021/3/29
	 * Time: 1:12
	 *
	 * @param Schedule $schedule
	 */
	public function load(Schedule $schedule) {
		$this->schedule($schedule);
	}
	
	/**
	 * 配置执行规则
	 *
	 * Date: 2021/3/28
	 * Time: 17:16
	 *
	 * @param Schedule $schedule
	 */
	protected function schedule(Schedule $schedule) {
	
	}
	
	/**
	 * 添加
	 *
	 * Date: 2021/3/28
	 * Time: 22:00
	 *
	 * @param string $command
	 * @param array  $parameters
	 * @param int    $timeType
	 * @param array  $timeData
	 * @return int
	 */
	public function add($command, array $parameters = [], $timeType = self::TIME_TYPE_EVERY_MINUTE, array $timeData = []) {
		$_data = [
			'cmd' => [
				'name' => $command,
				'params' => $parameters,
			],
			'time' => [
				'type' => $timeType,
				'time' => $timeData,
			],
		];
		
		$this->_list[] = $_data;
		
		// 返回taskId 任务id
		return count($this->_list) - 1;
	}
	
	/**
	 * 配置数据项 - 命令部分
	 *
	 * Date: 2021/3/28
	 * Time: 22:34
	 *
	 * @param string|\Closure $command
	 * @param array  $parameters
	 * @return int
	 */
	public function addDataCmd($command, array $parameters = []) {
		$_data = [
			'cmd'  => [
				'type'   => is_string($command) ? self::COMMAND_TYPE_COMMAND : self::COMMAND_TYPE_CLOSURE,
				'cmd'    => $command,
				'params' => $parameters,
			],
			'time' => [
				'type' => 0,
				'time' => [],
			],
		];
		
		$this->_list[] = $_data;
		
		// 返回taskId 任务id
		return count($this->_list) - 1;
	}
	
	/**
	 * 配置数据项 - 命令部分
	 *
	 * Date: 2021/3/28
	 * Time: 22:34
	 *
	 * @param string $command
	 * @param int    $timeType
	 * @param array  $timeData
	 * @return $this
	 */
	public function setDataTime($taskId, $timeType = self::TIME_TYPE_EVERY_MINUTE, array $timeData = []) {
		$_data = $this->_list[$taskId] ?? [];
		if (empty($_data)) {
			return $this;
		}
		
		$_data['time']['type'] = $timeType;
		$_data['time']['time'] = $timeData;
		
		$this->_list[$taskId] = $_data;
		
		return $this;
	}
	
	/**
	 * run
	 *
	 * Date: 2021/3/28
	 * Time: 22:01
	 *
	 * @param int $taskId 任务id
	 * @return false|\think\console\Output|\think\console\output\driver\Buffer
	 */
	public function run($taskId) {
		$_data = $this->_list[$taskId] ?? [];
		if (empty($_data)) {
			return false;
		}
		
		$_cmd = $_data['cmd'] ?? [];
		if (empty($_cmd)) {
			return false;
		}
		
		switch ($_cmd['type']) {
			case self::COMMAND_TYPE_COMMAND:
				return $this->runCommand($_cmd['cmd'], $_cmd['params']);
				break;
				
			case self::COMMAND_TYPE_CLOSURE:
				return call_user_func_array($_cmd['cmd'], $_cmd['params']);
				break;
		}
		
		return false;
	}
	
	/**
	 * 命令执行
	 *
	 * Date: 2021/3/28
	 * Time: 21:12
	 *
	 * @param string $command
	 * @param array  $parameters
	 * @return \think\console\Output|\think\console\output\driver\Buffer
	 */
	public function runCommand($command, array $parameters = []) {
		return Console::call($command, $parameters);
	}
	
	/******************************************************************
	 * get set
	 ******************************************************************/
	
	/**
	 * @return array
	 */
	public function &getList(): array {
		return $this->_list;
	}
	
	/**
	 * @param array $list
	 * @return $this
	 */
	public function setList(array $list) {
		$this->_list = $list;
		
		return $this;
	}
	
	
}