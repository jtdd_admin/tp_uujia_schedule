<?php


namespace uujia\schedule\data;


use uujia\schedule\config\ScheduleConfig;
use uujia\schedule\time\TimeComparator;
use uujia\schedule\traits\InstanceTrait;

/**
 * Class Schedule
 * Date: 2021/3/28
 * Time: 18:42
 *
 * @package uujia\schedule\data
 */
class Schedule {
	use InstanceTrait;
	
	/**
	 * @var ScheduleConfig
	 */
	protected $_scheduleConfig;
	
	/**
	 * 命令名称
	 * @var int
	 */
	protected $_taskId = 0;
	
	/**
	 * Schedule constructor.
	 *
	 * @param ScheduleConfig $scheduleConfig
	 */
	public function __construct(ScheduleConfig $scheduleConfig) {
		$this->_scheduleConfig = $scheduleConfig;
	}
	
	/**
	 * load
	 *
	 * Date: 2021/3/29
	 * Time: 1:12
	 *
	 * @return $this
	 */
	public function load() {
		$this->_scheduleConfig->load($this);
		
		return $this;
	}
	
	/**
	 * run
	 *
	 * Date: 2021/3/29
	 * Time: 1:09
	 *
	 * @return $this
	 */
	public function run() {
		foreach ($this->_scheduleConfig->getList() as $taskId => $item) {
			if (TimeComparator::getInstance()->comparate($taskId, $item)->isDue()) {
				$this->_scheduleConfig->run($taskId);
			}
		}
		
		return $this;
	}
	
	
	/**
	 * command
	 *
	 * Date: 2021/3/28
	 * Time: 21:12
	 *
	 * @param string $command
	 * @param array  $parameters
	 * @return $this
	 */
	public function command($command, array $parameters = []) {
		$this->_taskId = $this->_scheduleConfig->addDataCmd($command, $parameters);
		
		return $this;
	}
	
	/**
	 * call
	 *
	 * Date: 2021/3/28
	 * Time: 23:00
	 *
	 * @param \Closure $callback
	 * @param array    $parameters
	 * @return $this
	 */
	public function call($callback, array $parameters = []) {
		$this->_taskId = $this->_scheduleConfig->addDataCmd($callback, $parameters);
		
		return $this;
	}
	
	/**
	 * 每分钟
	 *
	 * Date: 2021/3/28
	 * Time: 23:17
	 *
	 * @return $this
	 */
	public function everyMinute() {
		$_timeData = [
			'minute' => 1,
		];
		
		$this->_scheduleConfig->setDataTime($this->_taskId, ScheduleData::TIME_TYPE_EVERY_MINUTE, $_timeData);
		
		return $this;
	}
	
	/**
	 * 每5分钟
	 *
	 * Date: 2021/3/28
	 * Time: 23:22
	 *
	 * @return $this
	 */
	public function everyFiveMinutes() {
		$_timeData = [
			'minute' => 5,
		];
		
		$this->_scheduleConfig->setDataTime($this->_taskId, ScheduleData::TIME_TYPE_EVERY_MINUTE, $_timeData);
		
		return $this;
	}
	
	/**
	 * 每10分钟
	 *
	 * Date: 2021/3/28
	 * Time: 23:22
	 *
	 * @return $this
	 */
	public function everyTenMinutes() {
		$_timeData = [
			'minute' => 10,
		];
		
		$this->_scheduleConfig->setDataTime($this->_taskId, ScheduleData::TIME_TYPE_EVERY_MINUTE, $_timeData);
		
		return $this;
	}
	
	/**
	 * 每30分钟
	 *
	 * Date: 2021/3/28
	 * Time: 23:22
	 *
	 * @return $this
	 */
	public function everyThirtyMinutes() {
		$_timeData = [
			'minute' => 30,
		];
		
		$this->_scheduleConfig->setDataTime($this->_taskId, ScheduleData::TIME_TYPE_EVERY_MINUTE, $_timeData);
		
		return $this;
	}
	
	/**
	 * 每小时
	 *
	 * Date: 2021/3/28
	 * Time: 23:22
	 *
	 * @return $this
	 */
	public function hourly() {
		$_timeData = [
			'hour' => 1,
		];
		
		$this->_scheduleConfig->setDataTime($this->_taskId, ScheduleData::TIME_TYPE_EVERY_HOUR, $_timeData);
		
		return $this;
	}
	
	/******************************************************************
	 * get set
	 ******************************************************************/
	
	/**
	 * @return int
	 */
	public function getTaskId(): int {
		return $this->_taskId;
	}
	
	/**
	 * @param int $taskId
	 * @return $this
	 */
	public function setTaskId(int $taskId) {
		$this->_taskId = $taskId;
		
		return $this;
	}
	
	
}