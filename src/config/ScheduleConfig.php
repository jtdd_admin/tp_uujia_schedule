<?php

namespace uujia\schedule\config;

use uujia\schedule\data\Schedule;
use uujia\schedule\data\ScheduleData;

/**
 * Class ScheduleConfig
 * Date: 2021/3/28
 * Time: 18:19
 *
 * @package uujia\schedule\config
 */
class ScheduleConfig extends ScheduleData {
	
	/**
	 * 配置执行规则
	 *
	 * Date: 2021/3/28
	 * Time: 17:16
	 *
	 * @param Schedule $schedule
	 */
	protected function schedule(Schedule $schedule) {
	
	}
	
}